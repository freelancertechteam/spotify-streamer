/**
 * Spotify Streamer, Copyright (C) 2015 FREELANCERTECH, www.freelancertech.net .
 * The Android Open Source Project
 * Author: Daniel Fouomene
 * EmailAuthor: daniel.fouomene@freelancertech.net, fouomenedaniel@gmail.com
 **/
package net.freelancertech.app.spotifystreamer;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.purplebrain.adbuddiz.sdk.AdBuddiz;

import net.freelancertech.app.spotifystreamer.R;

import net.freelancertech.app.spotifystreamer.fragment.ArtistFragment;


public class MainActivity extends ActionBarActivity {

    private final String LOG_TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // to display Icon launcher
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setLogo(R.mipmap.ic_launcher);
        actionBar.setDisplayUseLogoEnabled(true);
        // set title bar
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.action_bar_title_main);

        //Initialise pub
        AdBuddiz.setPublisherKey("8043497c-e0b3-439e-82e5-5b20845695cf");
        AdBuddiz.cacheAds(this); // this = current Activity

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new ArtistFragment())
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }


}
