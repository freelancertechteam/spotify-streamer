/**
 * Spotify Streamer, Copyright (C) 2015 FREELANCERTECH, www.freelancertech.net .
 * The Android Open Source Project
 * Author: Daniel Fouomene
 * EmailAuthor: daniel.fouomene@freelancertech.net, fouomenedaniel@gmail.com
 **/
package net.freelancertech.app.spotifystreamer.fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import net.freelancertech.app.spotifystreamer.R;


/**
 * A placeholder fragment containing a simple view.
 */
public class WebViewFragment extends Fragment {

    private static final String LOG_TAG = WebViewFragment.class.getSimpleName();
    public static final String TRACKS_URL = "URL";

    private String mUrl;
    private WebView mWebView;

    private static class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }


    public WebViewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Bundle arguments = getArguments();
        if (arguments != null) {
            mUrl = arguments.getString(WebViewFragment.TRACKS_URL);
            Log.d(LOG_TAG, "Url =" + mUrl);
        }

        // If there's instance state, mine it for useful information.
        // The end-goal here is that the user never knows that turning their device sideways
        // does crazy lifecycle related things.  It should feel like some stuff stretched out,
        // or magically appeared to take advantage of room, but data or place in the app was never
        // actually *lost*.
        if (savedInstanceState != null && savedInstanceState.containsKey(WebViewFragment.TRACKS_URL)) {
            // probably hasn't even been populated yet.  Actually perform the
            // swapout in onLoadFinished.
            mUrl = savedInstanceState.getParcelable(WebViewFragment.TRACKS_URL);
            Log.d(LOG_TAG, "Url Save =" + mUrl);
        }

        View rootView = inflater.inflate(R.layout.fragment_web_view, container, false);

        mWebView=(WebView)rootView.findViewById(R.id.webView);



        mWebView.getSettings().setLoadsImagesAutomatically(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mWebView.setWebViewClient(new MyBrowser());
        mWebView.loadUrl(mUrl);

        return rootView;

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // When tablets rotate, the currently selected list item needs to be saved.
        outState.putString(TRACKS_URL, mUrl);
        super.onSaveInstanceState(outState);
    }
}
