/**
 * Spotify Streamer, Copyright (C) 2015 FREELANCERTECH, www.freelancertech.net .
 * The Android Open Source Project
 * Author: Daniel Fouomene
 * EmailAuthor: daniel.fouomene@freelancertech.net, fouomenedaniel@gmail.com
 **/
package net.freelancertech.app.spotifystreamer;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import android.content.Intent;

import android.util.Log;

import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.Spotify;
import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerNotificationCallback;
import com.spotify.sdk.android.player.PlayerState;

import net.freelancertech.app.spotifystreamer.fragment.WebViewFragment;

import java.util.ArrayList;
import java.util.List;


public class WebViewActivity extends ActionBarActivity  implements
        PlayerNotificationCallback, ConnectionStateCallback {

    private static final String LOG_TAG = WebViewActivity.class.getSimpleName();

    // TODO: Replace with your client ID
    private static final String CLIENT_ID = "f4fbbd4c359a4a89834361ef6a8e5f35";
    // TODO: Replace with your redirect URI
    private  String REDIRECT_URI = "https://embed.spotify.com/?uri=";

    private Player mPlayer;

    // Request code that will be used to verify if the result comes from correct activity
    // Can be any integer
    private static final int REQUEST_CODE = 1337;

    private String mUrl = "https://embed.spotify.com/?uri=";
    private List<String> trackUris ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_web_view);

        ActionBar actionBar = getSupportActionBar();
        // to display Icon launcher
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setLogo(R.mipmap.ic_launcher);
        actionBar.setDisplayUseLogoEnabled(true);
        // set title bar
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.action_bar_title_web_view);

        trackUris = new ArrayList<String>();


        if (savedInstanceState == null) {
            // Create the player fragment and add it to the activity
            // using a fragment transaction.

            Bundle arguments = new Bundle();
            // mUri= getIntent().getData() ;
            mUrl = getIntent().getStringExtra(WebViewFragment.TRACKS_URL) ;
            String[] tab = mUrl.split(":");
            //log
            /*for (int i = 0; i<tab.length; i++ ) {
                Log.d(LOG_TAG,"tab["+i+"]="+tab[i]);
            }*/
            String[] tabTrack = tab[3].split(",");
            for (int j = 0; j<tabTrack.length; j++ ) {
                //Log.d(LOG_TAG,"tabTrack["+j+"]="+tabTrack[j]);
                //spotify:track:02M6vucOvmRfMxTXDUwRXu
                trackUris.add("spotify:track:"+tabTrack[j]);
            }

            REDIRECT_URI = REDIRECT_URI + getIntent().getStringExtra(WebViewFragment.TRACKS_URL) ;

            arguments.putString(WebViewFragment.TRACKS_URL,REDIRECT_URI);


           AuthenticationRequest.Builder builder =
                    new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI);
            builder.setScopes(new String[]{"user-read-private", "streaming"});
            AuthenticationRequest request = builder.build();

            AuthenticationClient.openLoginActivity(this, REQUEST_CODE, request);


            WebViewFragment fragment = new WebViewFragment();
            fragment.setArguments(arguments);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_web_view, fragment)
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_web_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
/*        if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        Log.e(LOG_TAG, "In methode OnActivityResult");

        // Check if result comes from the correct activity
        if (requestCode == REQUEST_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);
            if (response.getType() == AuthenticationResponse.Type.TOKEN) {
                Config playerConfig = new Config(this, response.getAccessToken(), CLIENT_ID);
                mPlayer = Spotify.getPlayer(playerConfig, this, new Player.InitializationObserver() {
                    @Override
                    public void onInitialized(Player player) {
                        mPlayer.addConnectionStateCallback(WebViewActivity.this);
                        mPlayer.addPlayerNotificationCallback(WebViewActivity.this);
                        //list of tracks
                        mPlayer.play(trackUris);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        Log.e(LOG_TAG, "Could not initialize player: " + throwable.getMessage());
                    }
                });
            }
        }
    }

    @Override
    public void onLoggedIn() {
        Log.d(LOG_TAG, "User logged in");
    }

    @Override
    public void onLoggedOut() {
        Log.d(LOG_TAG, "User logged out");
    }

    @Override
    public void onLoginFailed(Throwable error) {
        Log.d(LOG_TAG, "Login failed");
    }

    @Override
    public void onTemporaryError() {
        Log.d(LOG_TAG, "Temporary error occurred");
    }

    @Override
    public void onConnectionMessage(String message) {
        Log.d(LOG_TAG, "Received connection message: " + message);
    }

    @Override
    public void onPlaybackEvent(EventType eventType, PlayerState playerState) {
        Log.d(LOG_TAG, "Playback event received: " + eventType.name());
        switch (eventType) {
            // Handle event type as necessary
            default:
                break;
        }
    }

    @Override
    public void onPlaybackError(ErrorType errorType, String errorDetails) {
        Log.d(LOG_TAG, "Playback error received: " + errorType.name());
        switch (errorType) {
            // Handle error type as necessary
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        // VERY IMPORTANT! This must always be called or else you will leak resources
        Spotify.destroyPlayer(this);
        super.onDestroy();
    }

}
