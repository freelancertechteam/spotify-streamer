/**
 * Spotify Streamer, Copyright (C) 2015 FREELANCERTECH, www.freelancertech.net .
 * The Android Open Source Project
 * Author: Daniel Fouomene
 * EmailAuthor: daniel.fouomene@freelancertech.net, fouomenedaniel@gmail.com
 **/
package net.freelancertech.app.spotifystreamer;

import android.app.Application;
import android.test.ApplicationTestCase;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }
}