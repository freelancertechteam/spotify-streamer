![logoSpotifyColor300.png](https://bitbucket.org/repo/MLqrMo/images/4263801470-logoSpotifyColor300.png)![freelancertech.png](https://bitbucket.org/repo/pyayq7/images/1355630032-freelancertech.png)
# Description #

**Streamer for Spotify** Jouer n'importe quel musique, n'importe où, instantanément avec Streamer for Spotify. [Plus d'informations ici](http://freelancertech.net/index.php/nos-solutions/libres/spotify-streamer) et [Téléchargeable gratuitement sur Google Play](https://play.google.com/store/apps/details?id=net.freelancertech.app.spotifystreamer). 

Streamer for Spotify est développé en suivant les « **Best Practices** » du développement mobile de manière général et Android en particulier. Il constitue un bon départ pour les développeurs qui souhaitent se lancer dans le développement des applications mobiles sur système Android.
 
Streamer for Spotify est architecturée sur trois couches :

* La couche UI : basée sur les composants Android UI. 
* La couche métier: basée sur les Services et SyncAdapter Android.
* La couche d’accès aux données : basée sur un Content Provider Android (fournisseur de contenu) qui  interagit avec la base de donnée SQLite.

Streamer for Spotify met en œuvre les concepts suivants :

* Rich, Responsive Layouts pour Tablette et SmartPhone Android
* Android Permission System.
* HTTP requests sur les Web Services.
* Services et SyncAdapter Android.
* Cursor Loader, URI Matcher et Content Provider.
* Notifications, ShareActionProvider et Custom Views Android.
* SQLite databases et JUnit tests.
* Retrofit.
* Picasso.

# Capture d'écran #
![SpotifyStreamer_studio.png](https://bitbucket.org/repo/MLqrMo/images/162340179-SpotifyStreamer_studio.png)
![spotifystreamer_tablette.png](https://bitbucket.org/repo/MLqrMo/images/2107683822-spotifystreamer_tablette.png)
![spotifystreamer.png](https://bitbucket.org/repo/MLqrMo/images/2028737242-spotifystreamer.png)
![spotifystreamer_toptracks.png](https://bitbucket.org/repo/MLqrMo/images/4023562552-spotifystreamer_toptracks.png)
![spotifystreamer_player.png](https://bitbucket.org/repo/MLqrMo/images/4173190536-spotifystreamer_player.png)
![spotifystreamer_spotify.png](https://bitbucket.org/repo/MLqrMo/images/1625957980-spotifystreamer_spotify.png)